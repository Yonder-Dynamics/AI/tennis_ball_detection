<img src="assets/logo-without-text.png" width="240">

# Yonder Dynamics

# tennis-ball-detection

## Summary
This repo contains the datasets needed for the rover to have the ability to correctly identify the target object, a tennis ball. 

## Dependencies
* [pandas](https://pandas.pydata.org/) python >= 0.23.4
* [numpy](http://www.numpy.org/) >= 1.16.2



## Contents

utils: Contains the dataset resulting from training

zed-pcl: Intertwines the use of the ZED SDK along with the dataset

# ZED PCL

## Getting started

- First, download the latest version of the ZED SDK on [stereolabs.com](https://www.stereolabs.com).
- Make sure you have [PCL](https://github.com/PointCloudLibrary/pcl/releases) installed with its visualization module.
- For more information, read the ZED [API documentation](https://www.stereolabs.com/developers/documentation/API/).


### Prerequisites

- Windows 7 64bits or later, Ubuntu 16.04
- [ZED SDK **2.1**](https://www.stereolabs.com/developers/) and its dependency ([CUDA](https://developer.nvidia.com/cuda-downloads))

## Build the program


#### Build for Linux

Open a terminal in the sample directory and execute the following command:

    mkdir build
    cd build
    cmake ..
    make

## Run the program

- Navigate to the build directory and launch the executable file
- Or open a terminal in the build directory and run the sample :

        ./ZED\ with\ PCL [path to SVO file]

You can optionally provide an SVO file path (recorded stereo video of the ZED)


### Troubleshooting
 - PCL view can start zoomed in, displaying only green and red colors. To adjust the viewport, simply zoom out using your mouse wheel or keyboard shortcuts.

### Limitations
- The point cloud conversion to PCL format is time consuming and will affect application running time.






## Additional Notes
* N/A
