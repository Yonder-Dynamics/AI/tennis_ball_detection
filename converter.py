import pandas as pd
import numpy as np
import os
import urllib.request

#this file expects to have a 
  #train-images-boxable-with-rotation.csv (https://storage.googleapis.com/openimages/2018_04/train/train-images-boxable-with-rotation.csv)
  #train-annotations-bbox.csv (https://storage.googleapis.com/openimages/2018_04/train/train-annotations-bbox.csv)
  #class-descriptions-boxable.csv (https://storage.googleapis.com/openimages/2018_04/class-descriptions-boxable.csv)
# in the base directory.  These are
#labeled as Image IDs and boxes under the train column,
#and class names on the page https://storage.googleapis.com/openimages/web/download.html
#run in the base directory. 
#It will download all the images of tennis balls to the folder image_files, and put the bounding box data in text_files

class_to_find = "Tennis ball"


image_dir = os.path.join(".", "image_files")
text_dir = os.path.join(".", "text_files")

if not os.path.exists(image_dir):
  os.makedirs(image_dir)
if not os.path.exists(text_dir):
  os.makedirs(text_dir)


dataframe_dict = {}

def write_bounding_box(filename, dimensions):
  if(filename in dataframe_dict):
    df = dataframe_dict[filename]
    df.loc[len(df.index)] = ["tennis"] + dimensions
  else:
    df = pd.DataFrame({"class":[], "x":[], "y":[], "width":[], "height":[]})
    
    df.loc[0] = ["tennis"] + dimensions
    dataframe_dict[filename] = df



images = pd.read_csv("train-images-boxable-with-rotation.csv")
annotations = pd.read_csv("train-annotations-bbox.csv")
classes = pd.read_csv("class-descriptions-boxable.csv", header=None)

# Dropping unneeded columns
annotations = annotations[["ImageID", "LabelName", "XMin", "XMax", "YMin", "YMax"]]

#finding the class name of the tennis ball
class_name = classes.loc[classes[1] == class_to_find].iloc[0][0]

#taking all the tennis balls
annotations = annotations.loc[annotations["LabelName"] == class_name]

for index, series in annotations.iterrows(): 
  data = list(series) #"ImageID", "LabelName", "XMin", "XMax", "YMin", "YMax"
  write_bounding_box(data[0], list(data[2:]))

#all dataframes written

for filename, df in dataframe_dict.items():
  csv_file_name = filename + ".txt"
  df.to_csv(os.path.join(text_dir, csv_file_name), header=False, index=False)


#finding all test-images where it is a tennis ball
images = images[images["ImageID"].isin(annotations.iloc[:,0])]


number = 1
for index, series in images.iterrows(): 
  print("Downloading image {} of {}".format(number, len(images.index)))
  url = series[2]
  filename = series[0] + ".jpg"
  urllib.request.urlretrieve(url, filename=os.path.join(image_dir, filename))
  number += 1