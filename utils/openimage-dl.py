import os
import numpy as np
import pandas as pd
import urllib.request
from preprocess import preprocess

#this file expects to have a 
  #train-images-boxable-with-rotation.csv
  #test-images-with-rotation.csv
  #train-annotations-bbox.csv
  #test-annotations-bbox.csv
  #class-descriptions-boxable.csv

class_name = "Tennis ball"
class_id = 0

# create path relative to /.darknet
dn_path = "../darknet/data/tb/"

if not os.path.exists(dn_path):
  os.makedirs(dn_path)
  print("Created folder: {}".format(dn_path))

# Create dataframes for csvs
train_images = pd.read_csv("train-images-boxable-with-rotation.csv")
test_images = pd.read_csv("test-images-with-rotation.csv")

train_bbox = pd.read_csv("train-annotations-bbox.csv")
test_bbox = pd.read_csv("test-annotations-bbox.csv")

# Retrieving tennis ball MID to use for querying the image and annotation files
classes = pd.read_csv("class-descriptions-boxable.csv", header=None)
tb_mid = classes.loc[classes[1] == class_name].iloc[0][0]

# Dropping unneeded columns from the bounding box files
bb_cols = ["ImageID", "LabelName", "XMin", "XMax", "YMin", "YMax"]

train_bbox = train_bbox[bb_cols]
train_bbox = train_bbox[train_bbox["LabelName"] == tb_mid]

test_bbox = test_bbox[bb_cols]
test_bbox = test_bbox[test_bbox["LabelName"] == tb_mid]

# querying for tennis balls images
train_images = train_images[train_images["ImageID"].isin(train_bbox["ImageID"])]
train_images.reset_index(inplace=True)
test_images = test_images[test_images["ImageID"].isin(test_bbox["ImageID"])]
test_images.reset_index(inplace=True)

def dl_imgs(dn_path, imgs, list_filename, anns, process=True):
    with open(os.path.join(dn_path, filename), "w") as f:
        for index, series in df.iterrows(): 
            print("Downloading images: {} of {}".format(index + 1, len(df.index)))
            url = series["OriginalURL"]
            name = str(series["ImageID"]) + ".jpg"
            if(os.path.isfile(os.path.join(dn_path, name))):
                print("Skipping file - already exists")
                continue
            urllib.request.urlretrieve(url=url, filename=os.path.join(dn_path, name))
            f.write(os.path.join("data/tb", name) + "\n")     
    print("Finished download images to {}".format(dn_path))


def anns_to_file(path, df):
    for index, series in df.iterrows(): 
        width = series["XMax"] - series["XMin"]
        height = series["YMax"] - series["YMin"]
        x_cntr = (series["XMax"] + series["XMin"])/2.0
        y_cntr = (series["YMax"] + series["YMin"])/2.0
        data = [class_id, x_cntr, y_cntr, width, height]
        filename = str(series["ImageID"]) + ".txt"
        with open(os.path.join(path, filename), "w") as f:
            for attr in data:
                f.write(str(attr) + " ")
            f.write('\n')
        print("Finished writing to file {}".format(filename))

def download_images(path, filename, df):
    with open(os.path.join(dn_path, filename), "w") as f:
        for index, series in df.iterrows(): 
            print("Downloading images: {} of {}".format(index + 1, len(df.index)))
            url = series["OriginalURL"]
            name = str(series["ImageID"]) + ".jpg"
            urllib.request.urlretrieve(url=url, filename=os.path.join(dn_path, name))
            f.write(os.path.join("data/tb", name) + "\n")     
    print("Finished download images to {}".format(dn_path))


anns_to_file(dn_path, train_bbox)
anns_to_file(dn_path, test_bbox)

download_images(dn_path, "train.txt", df=train_images)
download_images(dn_path, "test.txt", df=test_images)
