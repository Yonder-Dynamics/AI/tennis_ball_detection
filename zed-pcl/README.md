## Getting started

- First, download the latest version of the ZED SDK on [stereolabs.com](https://www.stereolabs.com).
- Make sure you have [PCL](https://github.com/PointCloudLibrary/pcl/releases) installed with its visualization module.
- For more information, read the ZED [API documentation](https://www.stereolabs.com/developers/documentation/API/).


### Prerequisites

- Windows 7 64bits or later, Ubuntu 16.04
- [ZED SDK **2.1**](https://www.stereolabs.com/developers/) and its dependency ([CUDA](https://developer.nvidia.com/cuda-downloads))

## Build the program


#### Build for Linux

Open a terminal in the sample directory and execute the following command:

    mkdir build
    cd build
    cmake ..
    make

## Run the program

- Navigate to the build directory and launch the executable file
- Or open a terminal in the build directory and run the sample :

        ./ZED\ with\ PCL [path to SVO file]

You can optionally provide an SVO file path (recorded stereo video of the ZED)


### Troubleshooting
 - PCL view can start zoomed in, displaying only green and red colors. To adjust the viewport, simply zoom out using your mouse wheel or keyboard shortcuts.

### Limitations
- The point cloud conversion to PCL format is time consuming and will affect application running time.
